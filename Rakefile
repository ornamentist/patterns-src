require "html/proofer"


# Handy file and directory glob patterns

CLEAN_GLOB    = ""
CLOBBER_GLOB  = ""
THEMES_GLOB   = "../generate/galleries/*[0-9]*"
DS_STORE_GLOB = "./**/.DS_Store"


# Rsync helper

def rsync(src, dest)
  sh "rsync --delete -rtvu --exclude=source #{src} #{dest}"
end


# Galleries synchronization tasks

task :update_patterns do
  FileList[THEMES_GLOB].each do |dir|
    src = File.basename(dir)

    next if not File.exist?("_galleries/#{src}")

    directory "_galleries/#{src}/patterns"
    rsync("#{dir}/", "_galleries/#{src}/patterns")
  end
end


# Checking and validation tasks

task :check do
  options = {
    :favicon     => true,
    :check_html  => true,
    :href_ignore => [/parts\.ornamentist\.org/]
  }

  sh "jekyll build"

  HTML::Proofer.new("./deploy", options).run
end


# File cleaning

task :clean do
  sh "rm -f #{FileList[CLEAN_GLOB, DS_STORE_GLOB]}"
end

task :clobber => [:clean] do
  sh "rm -f #{FileList[CLOBBER_GLOB]}"
end


# Rake tasks

task :update  => [:update_patterns]

task :default => [:update]


