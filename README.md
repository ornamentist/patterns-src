# Pattern Parts Website Source

This repository contains the source files for the [Pattern Parts](http://parts.ornamentist.org) static website.


## Site Structure

The Pattern Parts site content is managed by the static website creator [Jekyll](https://jekyllrb.com/). Jekyll looks for publishable HTML, CSS and other content in directories like `_layouts`, `_plugins`, `_includes`. The galleries of patterns (SVG, PDF and related files) are all under the `_galleries` directory. I use [Rake](https://github.com/ruby/rake) to build and lint the site and copy publishable assets to the `deploy` directory.


## Github Pages and Jekyll

For sites managed with [Github pages](https://pages.github.com/), Github will automagically process any Jekyll-managed content it finds, including calling Jekyll plugins from a whitelist of tested plugins. Unfortunately Pattern Parts needs a bunch of custom Jekyll plugins (in the `_plugins` directory) for laying out and managing the galleries of patterns.

This means I need to generate the publishable assets myself and only push post-Jekyll content to the `gh-pages` branch. The `deploy` directory contains a `.nojekyll` file to tell Github not to further process the pushed files.


## Deploy Git Submodule

The publishable Pattern Parts assets are hosted by Github Pages. To keep the site publishable assets separate from the development and testing files, I manage the `deploy` directory as a [Git Submodule](http://git-scm.com/docs/git-submodule). This submodule contains only publishable assets ready to be pushed to the Github pages branch.


## SVG First Site

Where possible I have tried to use an 'SVG-first' approach to presenting the Pattern Parts pattern files. Each SVG pattern file has an associated set of files in EPS, PDF and PNG format. Pattern previews are SVG content, with external CSS rules applied as needed.


## Pattern Files

Pattern parts for the site are copied from an external directory into the `_galleries` directory tree as needed. Patterns are organized into numbered galleries of the twenty or so patterns per gallery. Custom Jekyll plugins take care of creating gallery HTML preview pages and a [whole-site catalog](http://parts.ornamentist.org/galleries/).


## The Future

### Managing pattern files

It's not clear that the static site approach I've used here will scale beyond a few hundred patterns--especially in terms of discovering and navigating pattern files. A future version of this site may need to find another approach or perhaps use an image-hosting service instead.


### Jekyll based site

Jekyll is good at creating blog-oriented static sites, but I feel I'm pushing against Jekyll's simplicity and strengths by using it for auto-generated galleries of patterns. Perhaps a site generator like [Middleman](https://middlemanapp.com/) might be a better match for the workflows here.


### Github pages hosting

The approach I've taken here of keeping deployable files in a Git submodule is clunky to say the least. Since I've done this to work within the Github pages and Jekyll publishing model it might be easier to host the site on a small VM or an image gallery hosting service.


## License

All source code and site assets are licensed under an MIT license (see the `LICENSE` file for details.) SVG, PNG, EPS and PDF files under the `_galleries` and `deploy` directories are licensed under a [Creative Commons](http://creativecommons.org/licenses/by/4.0/) license.


## Credits

The Patterns Parts site theme is based on a [Zurb Foundation](http://foundation.zurb.com/) theme I purchased [here](https://www.foundationmade.com/themes/item/FMwZh5tn). Site fonts are linked from [Google Fonts](https://www.google.com/fonts) and [Font Awesome](https://fortawesome.github.io/Font-Awesome/). The lightbox used to preview gallery images is [Fancybox](http://fancyapps.com/fancybox/).
