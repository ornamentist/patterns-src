require "liquid"
require_relative "helpers"


module Jekyll

  # Liquid tag for simple HTML gallery of SVG files.

  class SVGGallery < Liquid::Tag

    def initialize(tag, text, tokens)
      super

      # Split tag text for catalog parameters. Not robust!

      @params  = text.split(/\s*\/\/\s*/)

      @path    = (@params[0] || "").strip
      @class   = (@params[1] || "").strip
      @width   = (@params[2] || "200").strip.to_i
      @height  = (@params[3] || "200").strip.to_i
      @columns = (@params[4] || "4").strip.to_i

      # Setup Liquid template file

      liquid    = template_file(__dir__, "gallery")
      @template = File.read(liquid)
    end


    def render(context)

      # Find site root

      site = context.registers[:site]
      root = site.config["galleries"]
      root = "#{__dir__}/../#{root}"
      base = site.config["baseurl"]

      # Find gallery contents

      path  = normalize("#{root}/#{@path}")
      files = extns_in(path, "svg")

      return "" if files == []

      # Collate pattern files

      patterns = files.map.with_index do |svg, i|
        {"path"    => deploy_path(svg),
         "width"   => @width,
         "height"  => @height,
         "name"    => pattern_index(svg),
         "caption" => pattern_name(svg),
         "formats" => formats_of(svg)}
      end

      # Find possible gallery downloads

      theme = path.pathmap("%-1d")
      zip   = "#{path}/#{theme}.zip"
      pdf   = "#{path}/#{theme}-A4.pdf"

      # Generate gallery HTML from Liquid template

      parsed = Liquid::Template.parse(@template)

      parsed.render(\
        "context"  => deploy_ref(path),
        "base"     => base,
        "zip"      => deploy_path(zip),
        "pdf"      => deploy_path(pdf),
        "patterns" => patterns,
        "others"   => deployed_under(root))
    end
  end
end


Liquid::Template.register_tag('SVG_gallery', Jekyll::SVGGallery)
