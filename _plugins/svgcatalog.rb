require "liquid"
require_relative "helpers"


module Jekyll

  # Liquid tag for simplistic HTML gallery catalog

  class SVGCatalog < Liquid::Tag

    def initialize(tag, text, tokens)
      super

      # Split tag text for catalog parameters. Not robust!

      @params  = text.split(/\s*\/\/\s*/)
      @height  = (@params[0] || "110").strip.to_i
      @columns = (@params[1] || "4").strip.to_i

      # Setup Liquid template file

      liquid    = template_file(__dir__, "catalog")
      @template = File.read(liquid)
    end


    def render(context)

      # Find the galleries root directory

      site = context.registers[:site]
      root = site.config["galleries"]
      root = "#{__dir__}/../#{root}"
      base = site.config["baseurl"]

      # Collate gallery directories

      catalog = galleries_under(root).map do |dir|
        sample = sample_dir(dir, "svg")
        file   = deploy_path(sample)

        {"file"    => file,
         "dir"     => deploy_gallery(file),
         "base"    => base,
         "height"  => @height,
         "count"   => file_count(dir, "svg"),
         "caption" => gallery_name(dir)}
      end

      # Generate catalog HTML from Liquid template

      parsed = Liquid::Template.parse(@template)
      parsed.render("catalog" => catalog)
    end
  end
end


Liquid::Template.register_tag('SVG_catalog', Jekyll::SVGCatalog)
