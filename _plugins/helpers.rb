require "rake"


# Path to Liquid template file

def template_file(dir, name)
  "#{dir}/../_includes/#{name}-section.html"
end


# Normalized path name

def normalize(path)
  File.expand_path(path)
end


# Directories matching _glob_, under _root_

def dirs_under(root, glob)
  Dir["#{root}/#{glob}"].select do |path|
    File.directory?(path)
  end
end


# Gallery directories under _root_

def galleries_under(root)
  dirs = dirs_under(root, "**/patterns/")

  dirs.sort.map do |dir|
    normalize(dir)
  end
end


# Files with _ext_ under _root_

def files_under(root, ext)
  Dir["#{root}/**/*.#{ext}"].map do |dir|
    normalize(dir)
  end
end


# Randomly chosen file under _root_

def random_files(root, ext, count = 1)
  files_under(root, ext).shuffle.take(count)
end


# Directories in _dir_

def dirs_in(dir)
  Dir["#{dir}/*"].select do |path|
    File.directory?(path)
  end
end


# Entries in _dir_ with extension _ext_

def extns_in(dir, ext)
  Dir.glob("#{dir}/*.#{ext}").sort
end


# Files with full basename _name_

def related_files(name)
  Dir.glob("#{name.pathmap('%X')}.{svg,eps,pdf}").sort
end


# Count of files in _dir_ with extension _ext_

def file_count(dir, ext)
  extns_in(dir, ext).count
end


# Random file with extension _ext_ in _dir_

def sample_dir(dir, extn)
  extns_in(dir, extn).sample
end


# Deployable Jekyll path for _path_

def deploy_path(path)
  path.gsub(/^.*_/, "/")
end


# Deployable gallery directory name

def deploy_gallery(path)
  path.gsub(/\/patterns.*$/, "")
end


# Deployable path reference

def deploy_ref(path)
  deploy_gallery(deploy_path(path))
end


# Humanized mixed gallery name

def mixed_name(name)
  "Parts #{name.gsub(/0+(\d+)/, "\\1").capitalize}"
end


# Humanized thematic gallery name

def thematic_name(parts)
  "#{parts[1].capitalize} #{parts[2].gsub(/0+(\d+)/, "\\1")}"
end


# Humanized gallery directory name

def gallery_name(path)
  parts = path.pathmap("%-1d").split("-")

  if parts.count > 2
    thematic_name(parts)
  else
    mixed_name(parts[0])
  end
end


# Humanized gallery pattern index

def pattern_index(path)
  file = path.pathmap("%f").split(".")[0]
end


# Humanized gallery pattern name

def pattern_name(path)
  dir  = path.pathmap("%d").split("/")[-2]
  file = path.pathmap("%f").split(".")[0]

  "#{dir}: #{file}"
end


# All galleries under _root_ directory

def deployed_under(root)
  galleries_under(root).map do |path|
    deploy_ref(path)
  end
end


# Humanized file format name

def format_name(path)
  path.pathmap("%x")[1..-1].upcase
end


# Map of file formats in _dir_ for _name_

def formats_of(name)
  related_files(name).each_with_object({}) do |path, memo|
    memo[format_name(path)] = deploy_path(path)
  end
end
