require "liquid"
require_relative "helpers"


module Jekyll

  # Liquid tag for HTML list of random SVG files.

  class SVGRandom < Liquid::Tag

    def initialize(tag, text, tokens)
      super

      # Parse tag text for random parameters. Not robust!

      @count = (text || "1").strip.to_i

      # Setup Liquid template file

      liquid    = template_file(__dir__, "randoms")
      @template = File.read(liquid)
    end


    def render(context)

      # Find site root

      site = context.registers[:site]
      root = site.config["galleries"]
      root = "#{__dir__}/../#{root}"
      base = site.config["baseurl"]

      # Choose random SVG's

      files = random_files(root, "svg", @count).map do |file|
        {"file"    => deploy_path(file),
         "base"    => base,
         "gallery" => deploy_ref(file)}
      end

      # Generate random SVG HTML from Liquid template

      parsed = Liquid::Template.parse(@template)
      parsed.render("samples" => files)
    end
  end
end


Liquid::Template.register_tag('SVG_random', Jekyll::SVGRandom)
